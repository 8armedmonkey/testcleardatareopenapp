package com.eightam.android.cleardatareopenapp;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import static com.eightam.android.cleardatareopenapp.MessageActivity.getStartIntentNewTask;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonScheduleReopen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleReopen();
            }
        });

        findViewById(R.id.buttonClearData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scheduleReopen();
                clearData();
            }
        });
    }

    private void scheduleReopen() {
        Context appContext = getApplicationContext();

        PendingIntent operation = PendingIntent.getActivity(
                appContext, 0, getStartIntentNewTask(appContext), PendingIntent.FLAG_ONE_SHOT);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 5000, operation);
    }

    private void clearData() {
        ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        am.clearApplicationUserData();
    }

}
